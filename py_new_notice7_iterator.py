import random


class SimpleIterator:
    def __iter__(self):
        return self

    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return 1
        else:
            raise StopIteration

s_iter1 = SimpleIterator(4)
# print(next(s_iter1))
# print(next(s_iter1))
# print(next(s_iter1))
# print(next(s_iter1))

# print(s_iter1.__next__())
# print(s_iter1.__next__())
# print(s_iter1.__next__())
# # print(s_iter1.__next__())

test_list: list = [i for i in s_iter1]
print(test_list)

print('\n##############################################################')

def simple_generator(val):
   while val > 0:
       val -= 1
       yield random.choice(range(20)) 

gen_iter = simple_generator(5)
# print(next(gen_iter))
# print(next(gen_iter))
# print(next(gen_iter))
# print(next(gen_iter))
# print(next(gen_iter))
# print(next(gen_iter))

gen_list: list = [i for i in gen_iter]
print(gen_list)

print('\n##############################################################')

class TestIterator:
    def __iter__(self):
        return self
        
    def __init__(self, quantity):
        self.limit = quantity
        self.counter = 0

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return random.choice(range(10, 100))
        else:
            raise StopIteration

test_iter = TestIterator(10)
test_iter2 = iter(TestIterator(10))
test_iter_list: list = [i for i in test_iter]
test_iter_list2: list = [j for j in test_iter2]
print(test_iter_list)
print(test_iter_list2)


def matrix_generator(list1, list2):
    mat = []
    for i in list1:
        li = []
        for j in list2:
            li.append(f'{i}:{j}')
        mat.append(li)
    return mat	 

matrix_tl1_tl2 = matrix_generator(test_iter_list, test_iter_list2)
print(matrix_tl1_tl2)

def print_matrix(matrix):
	for e in matrix:
		for h in e:
			print(f'{h}  ', end='')
		print()

print_matrix(matrix_tl1_tl2)

